'use strict';

class CameraDevice {
    constructor() {
        /** Camera Options */
        this.deviceID;
        this.stream;
        this.track;
        this.recordingOptions  = {
            audio: false,
            video: {
                // facingMode : 'environment'
            }
        };

        /** A viewable stream of the video */
        this.video  = document.createElement( "video" );
        this.video.setAttribute( "playsinline", true );
        this.video.setAttribute( "autoplay"   , true );

        /** A canvas to use for computing stuff in the stream */
        this.canvas = document.createElement( "canvas" );
        this.canvas.width      = window.innerWidth;
        this.canvas.height     = window.innerHeight;
        this.ctx               = this.canvas.getContext( "2d" );

        /** Switch between Environment and Selfie Cameras */
        this.selfie = false;
        this.flip   = document.createElement( "button" );
        this.flip.innerHTML = "Flip Camera";
        this.flip.id = "btn_flipCamera";
        this.flip.addEventListener( 'click', ( e ) => {
            if( this.stream == null ) return
            this.stream.getTracks().forEach( t => {
                t.stop();
            } );
            this.selfie = !this.selfie;
            this.start();
        } );

        let supported = navigator.mediaDevices.getSupportedConstraints();
        if( supported[ "torch" ] ) {
            /** Toggle Torch/Flashlight On/Off */
            this.torch = false;
            this.flash = document.createElement( "button" );
            this.flash.innerHTML = "Use Flash";
            this.flash.id = "btn_useFlash";
            this.flip.style.gridColumn  = "1";
            this.flash.style.gridColumn = "2";
            this.flash.addEventListener( 'click', ( e ) => {
                this.stream.getTracks().forEach( t => {
                    t.stop();
                } );
                this.torch = !this.torch;
                this.start();
            } );
        }

        /** Switch between Environment and Selfie Cameras */
        // this.start();
    }

    start() {
        // this.recordingOptions.video = {
        //     facingMode : this.selfie ? 'user' : 'environment'
        // };
        navigator.mediaDevices
            .getUserMedia( this.recordingOptions )
            .then( ( stream ) => {
                this.video.srcObject = this.stream = stream;
                this.track = stream.getVideoTracks()[0];
                let supported = navigator.mediaDevices.getSupportedConstraints();
                if( supported[ "torch" ] ) {
                    this.video.addEventListener( 'loadedmetadata', () => {
                        this.track.applyConstraints( {
                            advanced: [ { torch: this.torch } ]
                        } );
                    } );
                }
                
            } )
            .catch( ( err )   => {
                console.log( err );
            } );
        return this;
    }
    
    show() {
        let supported = navigator.mediaDevices.getSupportedConstraints();
        let kids = [...document.body.children];
        if( kids.indexOf( this.video ) == -1 ) {
            document.body.appendChild( this.video );
        }
        if( kids.indexOf( this.flip ) == -1 ) {
            document.body.appendChild( this.flip );
        }
        if( supported[ "torch" ] && kids.indexOf( this.flash ) == -1 ) {
            document.body.appendChild( this.flash );
        }
        // this.video.play();
        return this;
    }

    freeze() {
        // this.video.pause();
        return this;
    }

    draw() {
        this.canvas.width    = window.innerWidth;
        this.canvas.height   = window.innerHeight;
        this.ctx.clearRect( 0, 0, this.canvas.width, this.canvas.height );
        this.ctx.drawImage( this.video, 0, 0, this.canvas.width, this.canvas.height );
        return this;
    }

    save( filename = "test", type  = "jpeg" ) {
        let link  = document.createElement( "a" );
        link.addEventListener( 'click', ( e ) => {
            this.draw();
            let img   = this.canvas.toDataURL( "image/" + type );
            link.href = img;
            link.setAttribute( "download", filename + "." + type );
        }, false );
        document.body.appendChild( link );
        // link.click();
        // document.body.removeChild( link );
        return this;
    }
}