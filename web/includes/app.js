// const video = document.getElementById( "camera" );
// video.width = document.body.clientWidth;
// video.height = window.innerHeight;

const cam        = new CameraDevice();
cam.video.width  = document.body.clientWidth;
cam.video.height = window.innerHeight;
cam.show();


let dispScale = {
  width  : document.body.clientWidth,
  height : window.innerHeight
};


Promise.all( [
  faceapi.nets.tinyFaceDetector.loadFromUri(   "./models" ),
  faceapi.nets.faceLandmark68Net.loadFromUri(  "./models" ),
  faceapi.nets.faceRecognitionNet.loadFromUri( "./models" ),
  faceapi.nets.faceExpressionNet.loadFromUri(  "./models" )
] ).then( cam.start() );

cam.video.addEventListener( "play", () => {

  const canvas  = faceapi.createCanvasFromMedia( cam.video );
  document.body.append( canvas );
  faceapi.matchDimensions( canvas, dispScale );

  window.addEventListener( "resize", () => {
    cam.video.width = document.body.clientWidth;
    cam.video.height = window.innerHeight;
    dispScale = {
      width  : document.body.clientWidth,
      height : window.innerHeight
    };
    faceapi.matchDimensions( canvas, dispScale );
  } );
  
  setInterval( async () => {

    const detections = await faceapi
      .detectAllFaces( cam.video, new faceapi.TinyFaceDetectorOptions() )
      .withFaceLandmarks()
      .withFaceExpressions();
    
    canvas
      .getContext( "2d" )
      .clearRect( 0, 0, canvas.width, canvas.height );
    
    const resizedDirections = faceapi.resizeResults( detections, dispScale );
    faceapi.draw.drawDetections( canvas, resizedDirections );
    faceapi.draw.drawFaceLandmarks( canvas, resizedDirections );

  }, 100 );

} );